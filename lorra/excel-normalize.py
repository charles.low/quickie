from openpyxl import load_workbook
import sys,os

def normalize_range(data):
    if not data:
        return None
    if '-' not in data:
        return data
    start,end = map(lambda x: x.strip(), data.split("-"))
    if ':' in start:
        sidx,start = map(lambda x: x.strip(), start.split(':',1))
    else:
        sidx = None
    if ':' in end:
        eidx,end = map(lambda x: x.strip(), end.split(':',1))
        if int(eidx) > int(sidx):
            return "%s:%s-%s:%s" % (sidx,start,eidx,end)
        elif int(eidx) == int(sidx):
            return "%s:%s-%s" % (sidx,start,end)
        else:
            raise Exception("Incorrect data: %s" % (data,))
    else:
        slen = len(start)
        elen = len(end)
    
        if slen > elen:
            end = start[:slen-elen] + end
        if sidx:
            return "%s:%s-%s" % (sidx,start,end)
        else:
            return "%s-%s" % (start,end)

def parse_cell(data):
    if not data:
        return None
    tokens = map(lambda x: x.strip(), data.split(","))
    return ", ".join(map(lambda x: normalize_range(x), tokens))

def process_file(filepath, output_dir="output"):
    start_cell = 'D3'
    wb = load_workbook(filename=filepath)
    ws = wb.active
    for row in ws.iter_rows(min_row=3,min_col=4,max_col=5):
        for cell in row:
            cell.value = parse_cell(cell.value)
    try:
        os.makedirs(output_dir)
    except:
        pass
    wb.save(os.path.join(output_dir,os.path.basename(filepath)))

def main():
    args = sys.argv
    for f in args[1:]:
        process_file(f)

if __name__ == "__main__":
    main()
