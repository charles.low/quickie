# excel-normalize.py
Help to fix idx:range formatting in excel files

## Usage
 - excel-normalize.py file1 [file2] [file3] ...
 - The script will read in all files and processed result will be in output directory with identical filename
