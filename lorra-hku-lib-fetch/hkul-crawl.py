#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import requests
from requestium import Session, Keys
from urllib.parse import quote
import sys,time
from openpyxl import Workbook
import json


""" 
Script to extract HKU Library search result based on author input 

1. We need to obtain the JWT token to perform ajax request

"""

class LibSession:
    def __init__(self):
        self.session = requests.session()
        self.session.verify = False
        self.libraries = {}

    def init_session(self):
        print(1)
        # 1.  get /primo-explore/search? - TBMCookie
        self.session.get('https://julac.hosted.exlibrisgroup.com/primo-explore/search?vl%28freeText0%29=%E4%B8%81%E6%97%A5%E6%98%8C&search_scope=My+Institution&mode=Basic&vid=HKU&displayMode=full&bulkSize=20&highlight=true&dum=true&query=any%2Ccontains%2C%E4%B8%81%E6%97%A5%E6%98%8C&displayField=all&tab=default_tab')
        print(2)
        # 2. get /primo_library/libweb/webservices/rest/v1/configuration/HKU - JSESSIONID
        r = self.session.get('https://julac.hosted.exlibrisgroup.com/primo_library/libweb/webservices/rest/v1/configuration/HKU')
        config = r.json()
        for k,v in config['institution-libraries'].items():
            self.libraries[k] = v['library-name']
        print(self.libraries)
        print(3)
        # 3. get /primo_library/libweb/webservices/rest/v1/guestJwt/HKU_ALMA?isGuest=true&lang=en_US&targetUrl= - JWT token -  add JWT token to header
        r = self.session.get('https://julac.hosted.exlibrisgroup.com/primo_library/libweb/webservices/rest/v1/guestJwt/HKU_ALMA?isGuest=true&lang=en_US&viewId=HKU')
        jwt_token = r.text.strip('"')
        self.session.headers.update({'Authorization': 'Bearer {}'.format(jwt_token)})
        # get /primo_library/libweb/primoExploreLogin - 302 to SSO - should not be required


    def search_author(self, author):
        """GET /primo_library/libweb/webservices/rest/primo-explore/v1/pnxs?
            blendFacetsSeparately=true&getMore=0&inst=HKU_ALMA&lang=en_US&limit=10&
            mode=Basic&newspapersActive=false&newspapersSearch=false&offset=0&
            pcAvailability=false&q=any,contains,%E4%B8%81%E6%97%A5%E6%98%8C&qExclude=&
            qInclude=&refEntryActive=false&rtaLinks=true&scope=My+Institution&skipDelivery=Y&sort=rank&tab=default_tab&vid=HKU
        """
        search_url='''https://julac.hosted.exlibrisgroup.com/primo_library/libweb/webservices/rest/primo-explore/v1/pnxs'''
        search_params_dict={'getMore': '0', 
                        'lang': 'en_US', 
                        'rtaLinks': 'true', 
                        'newspapersSearch': 'false', 
                        'vid': 'HKU', 
                        'refEntryActive': 'false',
                        'newspapersActive': 'false', 
                        'sort': 'rank', 
                        'pcAvailability': 'false', 
                        'skipDelivery': 'Y', 
                        'blendFacetsSeparately': 'true', 
                        'qInclude': '', 
                        'inst': 'HKU_ALMA', 
                        'mode': 'advanced', 
                        'tab': 'default_tab', 
                        'offset': '0', 
                        'limit': '100', 
                        'q': 'creator,contains,{},AND', 
                        'scope': 'My+Institution', 
                        'qExclude': ''}
        search_params_string='blendFacetsSeparately=true&getMore=0&inst=HKU_ALMA&lang=en_US&limit=1000&' \
            'mode=Basic&newspapersActive=false&newspapersSearch=false&offset=0&' \
            'pcAvailability=false&q=creator,exact,{},AND;facet_pfilter,exact,books,AND&qExclude=&' \
            'qInclude=facet_tlevel,exact,available%7C,%7Cfacet_rtype,exact,books&refEntryActive=false&rtaLinks=true&scope=My+Institution&skipDelivery=Y&sort=rank&tab=default_tab&vid=HKU'
        print(4)
        r = self.session.get(search_url,params=search_params_string.format(quote(author)))
        resp_json = r.json()
        result = {}
        result['info'] = resp_json['info']
        print(result['info'])
        result['books'] = []
        count = 0
        for doc in resp_json['docs']:
            count +=1
            print('processing doc #{}'.format(count))
            #print(doc)
            record = {}
            record['avail'] = {}
            for availlibrary in doc['pnx']['display']['availlibrary']:
                parsed = self.parse_availlibrary(availlibrary)
                record['avail'][parsed['L']] = parsed
            record['title'] = doc['pnx']['display']['title'][0]
            #record['call_number'] = doc['encrichment']['virtualBrowseObject']['callNumber']
            #print(record)
            if 'creator' in doc['pnx']['display']:
                record['creator'] = doc['pnx']['display']['creator']
            elif 'contributor' in doc['pnx']['display']:
                record['creator'] = doc['pnx']['display']['contributor']
            elif 'creatorcontrib' in doc['pnx']['facets']:
                record['creator'] = doc['pnx']['facets']['creatorcontrib']
            elif 'creatorcontrib' in doc['pnx']['search']:
                record['creator'] = doc['pnx']['search']['creatorcontrib']
            else:
                print(json.dumps(doc, indent=2, sort_keys=True, ensure_ascii=False))
                
            record['call_number'] = [ r.get('1','') + r.get('2','') for r in record['avail'].values() ]
            record['library'] = [ self.libraries[r] for r in record['avail'].keys() ]
            del record['avail']
            print(record)
            result['books'].append(record)

        return result
            

    def parse_availlibrary(self, availlibrary):
        # "$$IHKU_ALMA$$LFPS$$1FPS Books$$2([中] Z997 .D564 2012 )$$Savailable$$X852JULAC_HKU$$YFPS$$Zfpslc$$P1"
        parts = availlibrary.split('$$')[1:]
        ret = {}
        for p in parts:
            ret[p[0]] = p[1:]
        return ret

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: {} <namelist>".format(sys.argv[0]))
        print("The output will be generated as <namelist>-output.xlsx")
        exit()
    ls = LibSession()
    ls.init_session()
    wb = Workbook()
    ws = wb.active
    ws.append(['search','author','title','library','call number'])
    with open(sys.argv[1], 'r') as f:
        l = f.readline()
        while l.strip():
            collections = ls.search_author(l.strip())
            ws.append(['***', 'search result for {} : found {}'.format(l.strip(), collections['info']['total'])])
            for book in collections['books']:
                ws.append([l.strip(), ', '.join(book['creator']), book['title'], ', '.join(book['library']), ', '.join(book['call_number'])])
            l = f.readline()
    wb.save('{}-output.xlsx'.format(sys.argv[1]))

